import * as React from 'react';
import { LinkingOptions } from './types';
declare const LinkingContext: React.Context<{
    options: LinkingOptions | undefined;
}>;
export default LinkingContext;
